package com.nahavand.laodicea;


public enum VersionModel {
    AboutLaodice, Gallery, Support, InTheNews, Location, Acknowledgment, SourceCode
}
