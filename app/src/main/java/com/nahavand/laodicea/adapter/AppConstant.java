package com.nahavand.laodicea.adapter;

public class AppConstant {

    // Number of columns of Grid View
    public static final int NUM_OF_COLUMNS = 3;

    // Gridview image padding
    public static final int GRID_PADDING = 8; // in dp

    // SD card image directory
    public static final String GALLERY_DIR = "Gallery";

    public static final String ABOUT_DIR = "About";

    public static final double LONGITUDE = 34.1973911;

    public static final double LATITUDE = 48.3685478;

}
