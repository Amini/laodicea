package com.nahavand.laodicea.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import com.nahavand.laodicea.R;

public class SimpleRecyclerAdapter extends RecyclerView.Adapter<SimpleRecyclerAdapter.VersionViewHolder> {
    List<String> versionModels;
    public OnItemClickListener clickListener;

    public SimpleRecyclerAdapter(List<String> versionModels) {
        this.versionModels = versionModels;
    }

    public void setOnItemClickListener(OnItemClickListener myClickListener) {
        this.clickListener = myClickListener;
    }

    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerlist_item, viewGroup, false);
        VersionViewHolder versionViewHolder = new VersionViewHolder(view);
        view.setOnClickListener(versionViewHolder);
        return versionViewHolder;
    }

    @Override
    public void onBindViewHolder(VersionViewHolder versionViewHolder, int i) {
        versionViewHolder.title.setText(versionModels.get(i));
    }

    @Override
    public int getItemCount() {
            return versionModels == null ? 0 : versionModels.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    class VersionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cardItemLayout;
        TextView title;
        TextView subTitle;

        public VersionViewHolder(View itemView) {
            super(itemView);

            cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);
            title = (TextView) itemView.findViewById(R.id.listitem_name);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v, getLayoutPosition());
        }
    }
}
