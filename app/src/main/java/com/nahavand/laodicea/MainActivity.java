package com.nahavand.laodicea;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.nahavand.laodicea.adapter.SimpleRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private SimpleRecyclerAdapter simpleRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.animToolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle(getResources().getString( R.string.title_name));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.scrollableView);
        if(recyclerView == null) return;
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        List<String> listData = new ArrayList<>();
        for(VersionModel x : VersionModel.values())
            listData.add( ordinalToString(x));

        if (simpleRecyclerAdapter == null) {
            simpleRecyclerAdapter = new SimpleRecyclerAdapter(listData);
            simpleRecyclerAdapter.setOnItemClickListener(
                    new SimpleRecyclerAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            VersionModel v = VersionModel.values()[position];
                            switch (v)
                            {
                                case AboutLaodice:
                                    AboutLaodiceClicked();
                                    break;
                                case Gallery:
                                    GalleryClicked();
                                    break;
                                case Support:
                                    SupportClicked();
                                    break;
                                case InTheNews:
                                    InTheNewsClicked();
                                    break;
                                case Location:
                                    LocationClicked();
                                    break;
                                case Acknowledgment:
                                    AcknowledgmentClicked();
                                    break;
                                case SourceCode:
                                    SourceCodeClicked();
                                    break;
                            }
                        }
                    });
            recyclerView.setAdapter(simpleRecyclerAdapter);
        }
    }

    private void LocationClicked() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    private void AcknowledgmentClicked() {
        Intent intent = new Intent(this, AcknowledgementActivity.class);
        startActivity(intent);
    }

    private void InTheNewsClicked() {
        View view  = findViewById(R.id.mainLayout);
        assert view != null;
        Snackbar.make(view, "InTheNews Clicked", Snackbar.LENGTH_SHORT).show();
    }

    private void SupportClicked() {
        Intent intent = new Intent(this, SupportActivity.class);
        startActivity(intent);
    }

    private void GalleryClicked() {
        Intent intent = new Intent(this, ActivityGallery.class);
        startActivity(intent);
    }

    private void AboutLaodiceClicked() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void SourceCodeClicked() {
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/Amini/laodicea"));
        startActivity(intent);
    }

    private String ordinalToString(VersionModel x) {
        switch (x)
        {
            case AboutLaodice:
                return getResources().getString(R.string.AboutLaodicea);
            case Gallery:
                return getResources().getString(R.string.Gallery);
            case Support:
                return getResources().getString(R.string.Support);
            case InTheNews:
                return getResources().getString(R.string.InTheNews);
            case Location:
                return getResources().getString(R.string.Location);
            case Acknowledgment:
                return getResources().getString(R.string.Acknowledgment);
            case SourceCode:
                return getResources().getString(R.string.SourceCode);
        }
        return "";
    }

    public void floatingButtonOnClick(View view) {
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/joinchat/ClKatj9z-HTvz7QH6cqLMg"));
        startActivity(intent);
    }

}
